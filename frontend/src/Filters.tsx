import { Avatar, Chip, Grid, makeStyles, Paper, Slider, Typography, useTheme } from '@material-ui/core';
import * as React from 'react';
import { DateTimePicker } from '@material-ui/pickers';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import MomentUtils from '@date-io/moment';
// import DistanceFilter from './filters/Distance';

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'absolute',
        top: "20px",
        left: "20px",
        borderRadius: 15,
        display: 'flex',
        flexDirection: 'column',
        minWidth: '250px',
        padding: theme.spacing(3)
    },
    title: {
        color: theme.palette.primary.dark,
        fontWeight: 'bold',
        selfAlign: 'center'
    }
}))
const FilterLabel = ({ children: text }) => {
    const theme = useTheme();
    return <Typography variant="h6" style={{ color: theme.palette.primary.dark, fontWeight: 500 }}>{text}</Typography>
}

const Filter = () => {
    const { moment } = new MomentUtils();
    const [ dateValue, setDateValue ] = React.useState(moment());
    const classes = useStyles();

    const slider_default_value = (() => {
        const hour = dateValue.hour();
        // if (hour < 8 || hour > 22) {
        //     return -1;
        // }
        const minutes = dateValue.minutes();
        const m_i = minutes % 30;
        const slide_hour = hour - 8;
        return slide_hour + m_i;
    })();

    return (
        <Paper className={classes.root}>
            <Typography variant="h5" className={classes.title}>FILTROS</Typography>
            <div style={{ flexBasis: '20px' }} />
            <FilterLabel>Día</FilterLabel>
            <DateTimePicker value={dateValue} onChange={setDateValue} />
            <FilterLabel>Horario</FilterLabel>
            <Slider
                disabled={slider_default_value === -1 ? true : false}
                defaultValue={slider_default_value}
                valueLabelDisplay="auto"
                valueLabelFormat={(index) => {
                    const min_val = index % 2;
                    const hour_val = Math.floor(index/2);
                    const hour = hour_val+8;
                    const min = index === 0 ? '00' : '30';
                    return `${hour}:${min}`
                }}
                step={1}
                marks
                min={0}
                max={14*2}
            />
            <FilterLabel>Recado</FilterLabel>
            <Grid container >
                <Grid item xs={6}><Chip avatar={<Avatar>M</Avatar>} label="Farmacia" /></Grid>
                <Grid item xs={6}><Chip avatar={<Avatar>M</Avatar>} label="Supermercado" /></Grid>
            </Grid>
            {/* <DistanceFilter /> */}
        </Paper>
    )
}

export default Filter;
