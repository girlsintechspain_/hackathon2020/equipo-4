import * as React from 'react';
import mapboxgl from 'mapbox-gl';
import { makeStyles } from '@material-ui/core';
import markersvg from './images/marker.svg';

const useStyles = makeStyles({
    map: {
        height: '100%',
        width: '100%'
    }
})
const Map = ({ confinados }) => {
    const [map, setMap] = React.useState(null);
    const mapContainer = React.useRef(null);
    const confs = React.useState(confinados);
    const classes = useStyles();
  

    React.useEffect(() => {
        mapboxgl.accessToken = 'pk.eyJ1IjoidGV2dmVrIiwiYSI6ImNqbXc2MjBvZjE3cHcza3F5bGt6cGk2bnQifQ.Eg0yKWIiP2OeJss90olzEQ';
        const map = new mapboxgl.Map({
            container: mapContainer.current,
            style: "mapbox://styles/mapbox/streets-v11", // stylesheet location
            center: [2.0442543667869018, 41.37856695310515],
            zoom: 17,
        });
        map.on("load", () => {
          setMap(map);
          map.resize();
        });
        map.on("click", (ev) => {
            const lnglat = ev.lngLat;
            console.log(lnglat);
        })

        // new mapboxgl.Marker()
    }, []);

    // React.useEffect(() => {
    //     if (map != null) {
    //         confinados.map((conf) => {
    //             var el = document.createElement('div');
    //             el.className = 'marker';
    //             const marker = new mapboxgl.Marker(el).setLngLat([2.043452386291875, 41.380009481103656]).addTo(map);
    //         })
    //     }
    // }, [confinados, map])

    return <div ref={el => (mapContainer.current = el)} className={classes.map} />;
}

export default Map;